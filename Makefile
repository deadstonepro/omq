CXX=g++
CXXFLAGS=-c -Wall -std=c++17 -Iinclude
LD=g++
LD_FLAGS=

gio: main.o socket.o
	$(LD) $(LD_FLAGS) -o gio main.o socket.o

main.o: src/main.cpp include/queue.hpp
	$(CXX) $(CXXFLAGS) -o main.o src/main.cpp

socket.o: src/socket.cpp
	$(CXX) $(CXXFLAGS) -o socket.o src/socket.cpp

clean:
	rm -rf *.o gio
