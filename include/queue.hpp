#ifndef GIO_QUEUE_H_
#define GIO_QUEUE_H_

#include <queue>
#include <mutex>
#include <condition_variable>

namespace gio {
    template<typename T>
    class queue {
        std::queue<T> data;
        std::mutex mutex;
        std::condition_variable empty_cv;
    public:
        queue(const queue &) = delete;

        queue() { 
            // no-op
        }

        void push(T &&t) {
            std::unique_lock lock(mutex);
            data.push(std::move(t));
            empty_cv.notify_one();
            lock.unlock();
        }

        T pop() {
            std::unique_lock lock(mutex);
            empty_cv.wait(lock, [&](){return !data.empty();});
            auto result = std::move(data.front());
            data.pop();
            lock.unlock();
            return result;
        }
    };
}

#endif//GIO_QUEUE_H_
