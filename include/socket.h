#ifndef SOCKET_H_
#define SOCKET_H_

#include <memory>
#include <string>

namespace gio {

class Socket {
    int sock_fd;
    std::string host;
    std::string port;
public:
    using uptr = std::unique_ptr<Socket>;

    Socket
    (int sock_fd, std::string host, std::string port);

    ~Socket
    ();

    std::string hostname
    () const;
    
    Socket
    (const Socket &) = delete;

    void write
    (const std::string &data) const;

    std::string read
    (size_t buff_size=1024, bool wait_all=false) const;

    Socket::uptr accept
    () const;

    static Socket::uptr serve_tcp
    (std::string host, std::string port, unsigned int backlog);
};

}

#endif//SOCKET_H_
