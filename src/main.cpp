#include <iostream>
#include <thread>
#include <vector>

#include "gio.h"

using t_requests = gio::queue<gio::Socket::uptr>;

struct worker {
    unsigned int id;
    std::shared_ptr<t_requests> requests;

    worker
    (unsigned int id, std::shared_ptr<t_requests> requests):
        id(id), requests(requests) {
        std::cout << "Worker " << id << " has been started." << std::endl;
    }

    std::string dispatch
    (const std::string &request) {
        return request;
    }
    
    void operator()
    () {
        for (;;) {
            auto conn = requests->pop();
            auto resp = conn->read();
            conn->write(dispatch(resp));
        }
    }
};

struct acceptor {
    gio::Socket::uptr socket;
    std::shared_ptr<t_requests> requests;

    acceptor
    (gio::Socket::uptr _socket, std::shared_ptr<t_requests> requests)
        : socket(std::move(_socket)), requests(requests) {
        std::cout << "Start accepting on " << socket->hostname() << std::endl;
    }

    void operator()
    () {
        for (;;) {
            auto client = socket->accept();
            std::cout 
                << "Client " << client->hostname() 
                << " has connected." << std::endl;
            requests->push(std::move(client));
        }
    }
};

int main() {
    const std::string host = "127.0.0.1";
    const std::string port = "4000";
    const unsigned int backlog = 8;
    const unsigned int nworkers = 4;
    auto queue = std::make_shared<t_requests>();
    std::vector<std::thread> workers;
    for (unsigned int wid = 1; wid <= nworkers; ++wid) {
        std::thread t(worker(wid, queue));
        t.detach();
        workers.emplace_back(std::move(t));
    }
    auto server = gio::Socket::serve_tcp("127.0.0.1", "4000", backlog);
    acceptor(std::move(server), queue)();
    return 0;
}
