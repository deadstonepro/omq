#include <stdexcept>
#include <string>
#include <vector>
#include <iostream> // to delete
#include <sys/socket.h>
#include <arpa/inet.h> // sockaddr_in, inet_ntoa
#include <sys/errno.h> // errno
#include <unistd.h> // close
#include <netdb.h> // getprotobyname

#include "socket.h"

gio::Socket::Socket
(int sock_fd, std::string host, std::string port):
    sock_fd(sock_fd), host(host), port(port) {
    // no-op
}

gio::Socket::~Socket
() {
    close(sock_fd);
}

std::string gio::Socket::hostname
() const {
    return host + ":" + port;
}

gio::Socket::uptr gio::Socket::serve_tcp
(std::string host, std::string port, unsigned int backlog) {
    int err_code;
    int sock_fd;
    int opts;
    // Open the socket
    const struct protoent *tcp_proto = getprotobyname("tcp");
    sock_fd = socket(PF_INET, SOCK_STREAM, tcp_proto->p_proto);     
    if (sock_fd == -1) {
        throw std::runtime_error("Couldn't create the socket: " 
            + std::string(strerror(errno)));
    }
    
    // Set option to reuse address
    opts = 1;
    err_code = setsockopt(sock_fd, SOL_SOCKET,
        SO_REUSEADDR, &opts, sizeof(opts));
    if (err_code == -1) {
        close(sock_fd);
        throw std::runtime_error("Couldn't set option to the socket: " 
            + std::string(strerror(errno)));
    }

    // Resolve an address of the socket
    struct addrinfo hints;
    struct addrinfo *result;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family     = AF_INET;
    hints.ai_socktype   = SOCK_STREAM;
    hints.ai_flags      = AI_PASSIVE;
    err_code = getaddrinfo(host.c_str(), port.c_str(), &hints, &result);
    if (err_code == -1) {
        freeaddrinfo(result);
        close(sock_fd);
        throw std::runtime_error("Couldn't resolve the address: "
            + std::string(gai_strerror(err_code)));
    }

    // Bind the socket
    err_code = bind(sock_fd, result->ai_addr, result->ai_addrlen);
    if (err_code == -1) {
        freeaddrinfo(result);
        close(sock_fd);
        throw std::runtime_error("Couldn't bind the socket: "
            + std::string(strerror(errno)));
    }
 
    // Start listening
    err_code = listen(sock_fd, backlog);
    if (err_code == -1) {
        freeaddrinfo(result);
        close(sock_fd);
        throw std::runtime_error("Couldn't start listening: "
            + std::string(strerror(errno)));
    }

    freeaddrinfo(result);
    return std::make_unique<gio::Socket>(sock_fd, host, port);
}

std::string gio::Socket::read
(size_t buff_size, bool wait_all) const {
    std::vector<char> buff(buff_size);
    int flags = wait_all ? MSG_WAITALL : 0;
    ssize_t recv_len = ::recv(sock_fd, (void *)&buff[0], buff_size, flags);
    if (recv_len == -1) {
        throw std::runtime_error("Couldn't receive the message: "
            + std::string(strerror(errno)));
    }
    std::string result = std::string(buff.begin(), buff.begin() + recv_len);
    return result;
}

void gio::Socket::write
(const std::string &data) const {
    ssize_t sent_len = ::send(sock_fd, (void *)&data[0], data.size(), 0);
    if (sent_len == -1) {
        throw std::runtime_error("Couldn't send the message: "
            + std::string(strerror(errno)));
    }
}

gio::Socket::uptr gio::Socket::accept
() const {
    struct sockaddr_in  client_addr;
    socklen_t           client_addrlen = sizeof(client_addr);
    int client_sock_fd = ::accept(sock_fd, (sockaddr *)&client_addr, &client_addrlen);
    if (client_sock_fd == -1) {
        throw std::runtime_error("Couldn't accept the client: "
            + std::string(strerror(errno)));
    }
    return std::make_unique<gio::Socket>(
        client_sock_fd,
        inet_ntoa(client_addr.sin_addr),
        std::to_string(client_addr.sin_port));
}
